package jp.co.tis.tiw.chouseichan.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import jp.co.tis.tiw.chouseichan.dto.DataSourceDto;
import jp.co.tis.tiw.chouseichan.dto.EventDisplayDto;
import jp.co.tis.tiw.chouseichan.entity.Candidate;
import jp.co.tis.tiw.chouseichan.entity.Vote;

/**
 * {@link EventDisplayDto}のJSONシリアライザ。
 *
 * ハンズオン用に、Ant Designのテーブルに直接設定可能な形式にデータを編集する。
 */
public class EventDisplayDtoJsonSerializer extends JsonSerializer<EventDisplayDto> {

    @Override
    public void serialize(EventDisplayDto value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {

        gen.writeStartObject();

        gen.writeNumberField("eventId", value.getEventId());
        gen.writeStringField("eventName", value.getEventName());
        gen.writeStringField("description", value.getDescription());

        gen.writeArrayFieldStart("columns");
        gen.writeStartObject();
        gen.writeStringField("title", "名前");
        gen.writeStringField("dataIndex", "name");
        gen.writeStringField("key", "name");
        gen.writeEndObject();
        for (Candidate candidate : value.getColumns()) {
            gen.writeStartObject();
            gen.writeStringField("title", candidate.getDateTime());
            gen.writeNumberField("dataIndex", candidate.getCandidateId());
            gen.writeNumberField("key", candidate.getCandidateId());
            gen.writeEndObject();
        }
        gen.writeEndArray();

        gen.writeArrayFieldStart("dataSource");
        int key = 1;
        for (DataSourceDto dataSourceDto : value.getDataSource()) {
            gen.writeStartObject();
            gen.writeNumberField("key", key++);
            gen.writeStringField("name", dataSourceDto.getName());
            for (Vote vote : dataSourceDto.getVotes()) {
                gen.writeStringField(vote.getCandidateId().toString(), vote.getAnswer());
            }
            gen.writeEndObject();
        }
        gen.writeEndArray();

        gen.writeEndObject();
    }
}
