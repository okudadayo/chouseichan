package jp.co.tis.tiw.chouseichan.dao;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.boot.ConfigAutowireable;

import jp.co.tis.tiw.chouseichan.entity.Participant;

@ConfigAutowireable
@Dao
public interface ParticipantDao {

    @Insert
    int insert(Participant participant);
}
